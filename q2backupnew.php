<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Aurum Planet</title>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pannellum/2.5.6/pannellum.css" integrity="sha512-UoT/Ca6+2kRekuB1IDZgwtDt0ZUfsweWmyNhMqhG4hpnf7sFnhrLrO0zHJr2vFp7eZEvJ3FN58dhVx+YMJMt2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
      <link rel="stylesheet" type="text/css" href="style.css">
   </head>
   <body>
     
	 <script type="text/javascript">
function selectionChanged(value)
{
   document.getElementById("panorama-360-view").innerHTML = '';
    pannellum.viewer('panorama-360-view', {
            "type": "equirectangular",
            "panorama": value,
            "autoLoad": true,
            "autoRotate": -2,
            "title": value
        });
}
</script>
	 <div style="display:inline; color: red">Choose Location:</div>
<div style="align: right;display:inline;">
    <select onchange="selectionChanged(this.value);">
        <option value="https://pannellum.org/images/alma.jpg">Level 1</option>
        <option value="https://pannellum.org/images/jfk.jpg">Level 2</option>
        <option value="https://pannellum.org/images/alma.jpg">Level 3</option>
        <option value="https://pannellum.org/images/alma.jpg">Level 4</option>
    </select>
</div>
	 
      <div id="panorama-360-view">
         <div id="controls">
            <div class="ctrl" id="pan-up">&#9650;</div>
            <div class="ctrl" id="pan-down">&#9660;</div>
            <div class="ctrl" id="pan-left">&#9664;</div>
            <div class="ctrl" id="pan-right">&#9654;</div>
            <div class="ctrl" id="zoom-in">&plus;</div>
            <div class="ctrl" id="zoom-out">&minus;</div>
            <div class="ctrl" id="fullscreen">&#x2922;</div>
         </div>
      </div>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/pannellum/2.5.6/pannellum.js" integrity="sha512-EmZuy6vd0ns9wP+3l1hETKq/vNGELFRuLfazPnKKBbDpgZL0sZ7qyao5KgVbGJKOWlAFPNn6G9naB/8WnKN43Q==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
      <script>
         viewer = pannellum.viewer('panorama-360-view', {
         "autoLoad": true,
         "showControls": false,
         "autoLoad": true, 
         "autoRotate": -2,
         "compass": false,
         "default": {
         "firstScene": "circle",
         "author": "Aurum PropTech",
         "sceneFadeDuration": 1000
         },
         
         "scenes": {
         "circle": {
             "title": "Turf",
             "hfov": 110,
             "pitch": -3,
             "yaw": 117,
             "type": "equirectangular",
             "panorama": "images/5.jpg",
             "hotSpots": [
                 {
                     "pitch": -2.1,
                     "yaw": 132.9,
                     "type": "scene",
                     "text": "Next scene",
                     "sceneId": "house"
                 }
             ]
         },
         
         "house": {
             "title": "Turf",
             "hfov": 110,
             "yaw": 5,
             "type": "equirectangular",
             "panorama": "images/4.jpg",
             "hotSpots": [
                 {
                     "pitch": -0.6,
                     "yaw": 37.1,
                     "type": "scene",
                     "text": "Next Scene",
                     "sceneId": "circle",
                     "targetYaw": -23,
                     "targetPitch": 2
                 }
             ]
         }
         }
         });
         
         // Make buttons work
         document.getElementById('pan-up').addEventListener('click', function(e) {
         viewer.setPitch(viewer.getPitch() + 10);
         });
         document.getElementById('pan-down').addEventListener('click', function(e) {
         viewer.setPitch(viewer.getPitch() - 10);
         });
         document.getElementById('pan-left').addEventListener('click', function(e) {
         viewer.setYaw(viewer.getYaw() - 10);
         });
         document.getElementById('pan-right').addEventListener('click', function(e) {
         viewer.setYaw(viewer.getYaw() + 10);
         });
         document.getElementById('zoom-in').addEventListener('click', function(e) {
         viewer.setHfov(viewer.getHfov() - 10);
         });
         document.getElementById('zoom-out').addEventListener('click', function(e) {
         viewer.setHfov(viewer.getHfov() + 10);
         });
         document.getElementById('fullscreen').addEventListener('click', function(e) {
         viewer.toggleFullscreen();
         });
      </script>
   </body>
</html>