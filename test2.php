<link rel="stylesheet" href="https://cdn.pannellum.org/2.4/pannellum.css"/>
    <script type="text/javascript" src="https://cdn.pannellum.org/2.2/pannellum.js"></script>
    
<script type="text/javascript">
function selectionChanged(value)
{
   document.getElementById("panorama").innerHTML = '';
    pannellum.viewer('panorama', {
            "type": "equirectangular",
            "panorama": value,
            "autoLoad": true,
            "autoRotate": -2,
            "title": value
        });
}
</script>

<div style="display:inline; color: red">Choose Location:</div>
<div style="align: right;display:inline;">
    <select onchange="selectionChanged(this.value);">
        <option value="https://pannellum.org/images/alma.jpg">Level 1</option>
        <option value="https://pannellum.org/images/jfk.jpg">Level 2</option>
        <option value="https://pannellum.org/images/alma.jpg">Level 3</option>
        <option value="https://pannellum.org/images/alma.jpg">Level 4</option>
    </select>
</div>
<div id="panorama"></div>

<script type="text/javascript">
pannellum.viewer('panorama', {
            "type": "equirectangular",
            "panorama": "https://pannellum.org/images/alma.jpg",
            "autoLoad": true,
            "autoRotate": -2,
            "title": '',
        });
</script>