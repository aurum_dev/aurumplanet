// pannellum.viewer('panorama', {
//       "type": "equirectangular",
//       "panorama": "360-image.jpg",
//       "autoLoad": false
//   })


  pannellum.viewer('panorama-360-view', {  
    "autoLoad": true, 
    "autoRotate": -2,
    "compass": false,
    "default": {
        "firstScene": "circle",
        "author": "Aurum PropTech",
        "sceneFadeDuration": 1000
    },

    "scenes": {
        "circle": {
            "title": "Turf",
            "hfov": 110,
            "pitch": -3,
            "yaw": 117,
            "type": "equirectangular",
            "panorama": "360-image.jpg",
            "hotSpots": [
                {
                    "pitch": -2.1,
                    "yaw": 132.9,
                    "type": "scene",
                    "text": "Spring House or Dairy",
                    "sceneId": "house"
                }
            ]
        },

        "house": {
            "title": "Turf",
            "hfov": 110,
            "yaw": 5,
            "type": "equirectangular",
            "panorama": "4.jpg",
            "hotSpots": [
                {
                    "pitch": -0.6,
                    "yaw": 37.1,
                    "type": "scene",
                    "text": "Mason Circle",
                    "sceneId": "circle",
                    "targetYaw": -23,
                    "targetPitch": 2
                }
            ]
        }
    }
})