
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <title></title>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta name="robots" content="noindex, nofollow">
  <meta name="googlebot" content="noindex, nofollow">
  <meta name="viewport" content="width=device-width, initial-scale=1">


  <script
    type="text/javascript"
    src="//code.jquery.com/jquery-2.1.3.js"
    
  ></script>

    <link rel="stylesheet" type="text/css" href="/css/result-light.css">

      <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

  <style id="compiled-css" type="text/css">
      .explore-images .neighborhood-listing,
.explore-images .leasing-plan {
    visibility: hidden;
    position: absolute;
    height: 400px;
    width: 100%;
}
.explore-images .neighborhood-listing.visible,
.explore-images .leasing-plan.visible {
	display: block;
    visibility: visible;
}

.explore {
	padding: 2.5% 5%;
	background-color: #f2efef;
}
.explore h4 {
	margin-top: 0;
	margin-bottom: .5em;
}

/* .directory-btns, */
.neighborhood-btns {
	position: relative;
	top: 0;
	right: 0;
}

@media screen and (max-width: 480px) {
	.desktop {
		display: none;
	}
	.mobile {
		display: block;
	}
}

#map-dining, #map-shopping, #map-schools, #map-parks, #map-hospitals, #map-colleges{
	width:100%;
	height: 400px;
}
    /* EOS */
  </style>

  <script id="insert"></script>


    <script src="/js/stringify.js?1de1b4f966a5c96afa94e8560c714e6cae481049" charset="utf-8"></script>
    <script>
      const customConsole = (w) => {
        const pushToConsole = (payload, type) => {
          w.parent.postMessage({
            console: {
              payload: stringify(payload),
              type:    type
            }
          }, "*")
        }

        w.onerror = (message, url, line, column) => {
          // the line needs to correspond with the editor panel
          // unfortunately this number needs to be altered every time this view is changed
          line = line - 70
          if (line < 0){
            pushToConsole(message, "error")
          } else {
            pushToConsole(`[${line}:${column}] ${message}`, "error")
          }
        }

        let console = (function(systemConsole){
          return {
            log: function(){
              let args = Array.from(arguments)
              pushToConsole(args, "log")
              systemConsole.log.apply(this, args)
            },
            info: function(){
              let args = Array.from(arguments)
              pushToConsole(args, "info")
              systemConsole.info.apply(this, args)
            },
            warn: function(){
              let args = Array.from(arguments)
              pushToConsole(args, "warn")
              systemConsole.warn.apply(this, args)
            },
            error: function(){
              let args = Array.from(arguments)
              pushToConsole(args, "error")
              systemConsole.error.apply(this, args)
            },
            system: function(arg){
              pushToConsole(arg, "system")
            },
            clear: function(){
              systemConsole.clear.apply(this, {})
            },
            time: function(){
              let args = Array.from(arguments)
              systemConsole.time.apply(this, args)
            },
            assert: function(assertion, label){
              if (!assertion){
                pushToConsole(label, "log")
              }

              let args = Array.from(arguments)
              systemConsole.assert.apply(this, args)
            }
          }
        }(window.console))

        window.console = { ...window.console, ...console }

        console.system("Running fiddle")
      }

      if (window.parent){
        customConsole(window)
      }
    </script>
</head>
<body>
    <div class="explore">
     <h4>Explore Your New Neighborhood</h4>

    <select id="neighborhood-select">
        <option value="Dining">Dining</option>
        <option value="Shopping">Shopping</option>
        <option value="Schools">Schools</option>
        <option value="Parks">Parks</option>
        <option value="Hospitals">Hospitals</option>
        <option value="Colleges">Colleges</option>
    </select>
</div>
<div class="explore-images featured-image">
    <div class="neighborhood-listing Dining visible">
        <div class="neighborhood-btns">
            <div id="map-dining"></div>
        </div>
    </div>
    <div class="neighborhood-listing Shopping ">
        <div class="neighborhood-btns">
            <div id="map-shopping"></div>
        </div>
    </div>
    <div class="neighborhood-listing Parks ">
        <div class="neighborhood-btns">
            <div id="map-parks"></div>
        </div>
    </div>
    <div class="neighborhood-listing Hospitals ">
        <div class="neighborhood-btns">
            <div id="map-hospitals"></div>
        </div>
    </div>
    <div class="neighborhood-listing Colleges ">
        <div class="neighborhood-btns">
            <div id="map-colleges"></div>
        </div>
    </div>
    <div class="neighborhood-listing Schools ">
        <div class="neighborhood-btns">
            <div id="map-schools"></div>
        </div>
    </div>
</div>

    <script type="text/javascript">//<![CDATA[


jQuery(document).ready(function ($) {
    $('#leasing-select, #neighborhood-select, #store-directories-select').change(function () {
        var new_image = $(this).val();
        $('.explore-images .visible').fadeOut().removeClass('visible');
        $('.explore-images .' + new_image).fadeIn(function () {
            resizeMap(new_image);
        }).addClass('visible');

    });
});

function resizeMap(type) {
    var typeL = type.toLowerCase();
    switch (typeL) {
        case 'dining':
            google.maps.event.trigger(map, 'resize');
            map.setCenter(mapOptions.center);
            break;
        case 'shopping':
            google.maps.event.trigger(map2, 'resize');
            map2.setCenter(mapOptions.center);
            break;
        case 'schools':
            google.maps.event.trigger(map3, 'resize');
            map3.setCenter(mapOptions.center);
            break;
        case 'parks':
            google.maps.event.trigger(map4, 'resize');
            map4.setCenter(mapOptions.center);
            break;
        case 'hospitals':
            google.maps.event.trigger(map5, 'resize');
            map5.setCenter(mapOptions.center);
            break;
        case 'colleges':
            google.maps.event.trigger(map6, 'resize');
            map6.setCenter(mapOptions.center);
            break;
        default:
            break;
    }
}

var map, map2, map3, map4, map5, map6;
var mapOptions;

function initialize() {
    var myCenter = new google.maps.LatLng(39.458342, -74.633672);


    var locations = [
        ['Cavallino Nero', 39.456832, -74.660559, 2],
        ['IHOP', 39.453284, -74.651666, 3],
        ['Longhorn Steakhouse', 39.453224, -74.641467, 4],
        ['Chilis Grill and Bar', 39.450375, -74.639798, 5],
        ['Applebees', 39.452392, -74.631073, 6]
    ];

    var locationsA = [
        ['Cavallino Nero', 39.456832, -74.660559, 2],
        ['IHOP', 39.453284, -74.651666, 3],
        ['Longhorn Steakhouse', 39.453224, -74.651666, 4],
        ['Chilis Grill and Bar', 39.450375, -74.639798, 5],
        ['Applebees', 39.452392, -74.631073, 6]
    ];

    var locationsB = [
        ['Cavallino Nero', 39.456832, -74.660559, 2],
        ['IHOP', 39.453284, -74.651666, 3],
        ['Longhorn Steakhouse', 39.453224, -74.651666, 4],
        ['Chilis Grill and Bar', 39.450375, -74.639798, 5],
        ['Applebees', 39.452392, -74.631073, 6]
    ];

    var locationsC = [
        ['Cavallino Nero', 39.456832, -74.660559, 2],
        ['IHOP', 39.453284, -74.651666, 3],
        ['Longhorn Steakhouse', 39.453224, -74.651666, 4],
        ['Chilis Grill and Bar', 39.450375, -74.639798, 5],
        ['Applebees', 39.452392, -74.631073, 6]
    ];

    var locationsD = [
        ['Cavallino Nero', 39.456832, -74.660559, 2],
        ['IHOP', 39.453284, -74.651666, 3],
        ['Longhorn Steakhouse', 39.453224, -74.651666, 4],
        ['Chilis Grill and Bar', 39.450375, -74.639798, 5],
        ['Applebees', 39.452392, -74.631073, 6]
    ];

    var locationsE = [
        ['Cavallino Nero', 39.456832, -74.660559, 2],
        ['IHOP', 39.453284, -74.651666, 3],
        ['Longhorn Steakhouse', 39.453224, -74.651666, 4],
        ['Chilis Grill and Bar', 39.450375, -74.639798, 5],
        ['Applebees', 39.452392, -74.631073, 6]
    ];

    mapOptions = {
        center: new google.maps.LatLng(39.458342, -74.633672),
        zoom: 13
    };

    map = new google.maps.Map(document.getElementById("map-dining"), mapOptions);
    map2 = new google.maps.Map(document.getElementById("map-shopping"), mapOptions);
    map3 = new google.maps.Map(document.getElementById("map-schools"), mapOptions);
    map4 = new google.maps.Map(document.getElementById("map-parks"), mapOptions);
    map5 = new google.maps.Map(document.getElementById("map-hospitals"), mapOptions);
    map6 = new google.maps.Map(document.getElementById("map-colleges"), mapOptions);

    /* causing Uncaught RangeError: Maximum call stack size exceeded 
    google.maps.event.addListener(map, 'resize', function () {
        // var center = map.getCenter();
        google.maps.event.trigger(map, "resize");
        map.setCenter(mapOptions.center);
    }); */

    var infowindow = new google.maps.InfoWindow();
    var marker, markerA, markerB, markerC, markerC, markerD, markerE, i;

    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            map: map,
            icon: 'http://maps.google.com/intl/en_us/mapfiles/ms/micons/restaurant.png'
        });
        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
                infowindow.setContent(locations[i][0]);
                infowindow.open(map, marker);
            }
        })(marker, i));
    }
    for (i = 0; i < locationsA.length; i++) {
        markerA = new google.maps.Marker({
            position: new google.maps.LatLng(locationsA[i][1], locationsA[i][2]),
            map: map2,
            icon: 'http://maps.google.com/intl/en_us/mapfiles/ms/micons/purple-dot.png'
        });
        google.maps.event.addListener(markerA, 'click', (function (markerA, i) {
            return function () {
                infowindow.setContent(locationsA[i][0]);
                infowindow.open(map2, markerA);
            }
        })(markerA, i));
    }
    for (i = 0; i < locationsB.length; i++) {
        markerB = new google.maps.Marker({
            position: new google.maps.LatLng(locationsB[i][1], locationsB[i][2]),
            map: map3,
            icon: 'http://maps.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png'
        });
        google.maps.event.addListener(markerB, 'click', (function (markerB, i) {
            return function () {
                infowindow.setContent(locationsB[i][0]);
                infowindow.open(map3, markerB);
            }
        })(markerB, i));
    }
    for (i = 0; i < locationsC.length; i++) {
        markerC = new google.maps.Marker({
            position: new google.maps.LatLng(locationsC[i][1], locationsC[i][2]),
            map: map4,
            icon: 'http://maps.google.com/intl/en_us/mapfiles/ms/micons/tree.png'
        });
        google.maps.event.addListener(markerC, 'click', (function (markerC, i) {
            return function () {
                infowindow.setContent(locationsC[i][0]);
                infowindow.open(map4, markerC);
            }
        })(markerC, i));
    }
    for (i = 0; i < locationsD.length; i++) {
        markerD = new google.maps.Marker({
            position: new google.maps.LatLng(locationsD[i][1], locationsD[i][2]),
            map: map5,
            icon: 'http://maps.google.com/intl/en_us/mapfiles/ms/micons/yellow-dot.png'
        });
        google.maps.event.addListener(markerD, 'click', (function (markerD, i) {
            return function () {
                infowindow.setContent(locationsD[i][0]);
                infowindow.open(map5, markerD);
            }
        })(markerD, i));
    }
    for (i = 0; i < locationsE.length; i++) {
        markerE = new google.maps.Marker({
            position: new google.maps.LatLng(locationsE[i][1], locationsE[i][2]),
            map: map6,
            icon: 'http://maps.google.com/intl/en_us/mapfiles/marker_whiteA.png'
        });
        google.maps.event.addListener(markerE, 'click', (function (markerE, i) {
            return function () {
                infowindow.setContent(locationsE[i][0]);
                infowindow.open(map6, markerE);
            }
        })(markerE, i));
    }

    var marker1 = new google.maps.Marker({
        position: myCenter,
        map: map,
        title: "Evergreen"
    });
    var marker2 = new google.maps.Marker({
        position: myCenter,
        map: map2,
        title: "Evergreen"
    });
    var marker3 = new google.maps.Marker({
        position: myCenter,
        map: map3,
        title: "Evergreen"
    });
    var marker4 = new google.maps.Marker({
        position: myCenter,
        map: map4,
        title: "Evergreen"
    });
    var marker5 = new google.maps.Marker({
        position: myCenter,
        map: map5,
        title: "Evergreen"
    });
    var marker6 = new google.maps.Marker({
        position: myCenter,
        map: map6,
        title: "Evergreen"
    });

}

google.maps.event.addDomListener(window, 'load', initialize);


  //]]></script>

  <script>
    // tell the embed parent frame the height of the content
    if (window.parent && window.parent.parent){
      window.parent.parent.postMessage(["resultsFrame", {
        height: document.body.getBoundingClientRect().height,
        slug: "fbt9p701"
      }], "*")
    }

    // always overwrite window.name, in case users try to set it manually
    window.name = "result"
  </script>

    <script>
      let allLines = []

      window.addEventListener("message", (message) => {
        if (message.data.console){
          let insert = document.querySelector("#insert")
          allLines.push(message.data.console.payload)
          insert.innerHTML = allLines.join(";\r")

          let result = eval.call(null, message.data.console.payload)
          if (result !== undefined){
            console.log(result)
          }
        }
      })
    </script>

</body>
</html>
